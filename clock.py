from flask import Flask, Response
import time
app = Flask(__name__)

#app.register_blueprint(sse, url_prefix='/streams')


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/whoami")
def who_am_i():
    return Response("Petrov Artem", mimetype="text")


@app.route("/gettime")
def what_time_is_it():
    def event_stream():
        while True:
            yield get_time() + "\n"
    return Response(event_stream(), mimetype="text/event-stream")


def get_time():
    time.sleep(1)
    s = time.ctime(time.time())
    return s


def bootapp():
    app.run(debug=True, port=8080)


if __name__ == "__main__":
    bootapp()
